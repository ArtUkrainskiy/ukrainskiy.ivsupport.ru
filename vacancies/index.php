<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Вакансии");
?><?$APPLICATION->IncludeComponent(
	"main:catalog.main.vacancy", 
	".default", 
	array(
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"DETAIL_URL" => "/vacancy/#ELEMENT_ID#/",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "vacancies",
		"PARENT_SECTION" => "",
		"SEF_FOLDER" => "/vacancy/",
		"SEF_MODE" => "Y",
		"COMPONENT_TEMPLATE" => ".default",
		"SEF_URL_TEMPLATES" => array(
			"detail" => "#ELEMENT_ID#/",
			"resume" => "#ELEMENT_ID#/resume/",
		)
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>