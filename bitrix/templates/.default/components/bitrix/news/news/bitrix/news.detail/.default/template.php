<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
if($arResult["PROPERTIES"]["BROWSER_TITLE"]["VALUE"]){
	$APPLICATION->SetTitle($arResult["PROPERTIES"]["BROWSER_TITLE"]["VALUE"]);
}
if($arResult["IPROPERTY_VALUES"]["KEYWORDS"]["VALUE"]){
	$APPLICATION->SetPageProperty('keywords', $arResult["PROPERTIES"]["KEYWORDS"]["VALUE"]);
}
?>
<?
$this->SetViewTarget("news_detail_date");
echo "<span class='main_date'>{$arResult["DISPLAY_ACTIVE_FROM"]}</span>";
$this->EndViewTarget();

?>
<?if(is_array($arResult["DETAIL_PICTURE"])):?>
	<img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" align="left" alt="<?=$arResult["NAME"]?>"/>
<?endif;?>
<?=$arResult["DETAIL_TEXT"];?>
<br>
