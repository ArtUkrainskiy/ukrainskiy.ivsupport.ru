<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script type="text/javascript" >
	$().ready(function(){
		$(function(){
			$('#slides').slides({
				preload: true,
				generateNextPrev: false,
				autoHeight: true,
				play: 4000,
				effect: 'fade'
			});
		});
	});
</script>

<div class="sl_slider" id="slides">
	<div class="slides_container">
		<?foreach($arResult["ITEMS"] as $arItem):?>
		<div>
			<div>
				<?if(is_array($arItem["DETAIL_PICTURE"])):?>
				<img src="<?=$arItem["DETAIL_PICTURE"]["src"]?>" alt="" />
				<?endif;?>
				<h2><a href="<?=$arItem["PROPERTY_LINK_DETAIL_PAGE_URL"]?>" title="<?=$arItem["PROPERTY_LINK_NAME"]?>"><?echo $arItem["NAME"]?></a></h2>
				<p><?=$arItem["PROPERTY_LINK_NAME"]?> всего за <?=$arItem["PROPERTY_LINK_PROPERTY_PRICE_VALUE"]?> руб.</p>
				<a href="<?=$arItem["PROPERTY_LINK_DETAIL_PAGE_URL"]?>" title="<?=$arItem["PROPERTY_LINK_NAME"]?>" class="sl_more">Подробнее &rarr;</a>
			</div>
		</div>
		<?endforeach;?>
	</div>
</div>

