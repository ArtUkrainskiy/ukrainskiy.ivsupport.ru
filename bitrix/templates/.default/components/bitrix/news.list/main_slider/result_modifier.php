<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach($arResult["ITEMS"] as $ID=>$arItem){
    $arImage = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"], Array("width" => $arParams["PREV_IMAGE_W"], "height" => $arParams["PREV_IMAGE_H"]), BX_RESIZE_IMAGE_PROPORTIONAL, true);
    $arResult["ITEMS"][$ID]["DETAIL_PICTURE"] = $arImage;
}

?>
