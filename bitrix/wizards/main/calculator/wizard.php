<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class SelectOperation extends CWizardStep{

    // Инициализация
    function InitStep(){
        // ID шага
        $this->SetStepID("select_operation");
        // Заголовок
        $this->SetTitle("Операция");
        // Навигация
        $this->SetNextStep("input_left_operand");
    }
    function ShowStep(){
        $arValues = Array(
            "+" => "+",
            "-" => "-",
            "*" => "*",
            "/" => "/",
        );
        $this->content .= "Выберите операцию: ";
        $this->content .= $this->ShowSelectField("operator", $arValues, false);
    }
}
class InputLeftOperand extends CWizardStep{

    // Инициализация
    function InitStep(){
        // ID шага
        $this->SetStepID("input_left_operand");
        // Заголовок
        $this->SetTitle("Правый операнд");
        // Навигация
        $this->SetPrevStep("select_operation");
        $this->SetNextStep("input_right_operand");
    }

    function ShowStep(){
        $this->content .= "Введите первое число: ";
        $this->content .= $this->ShowInputField("text", "left_operand");
    }

    function OnPostForm(){
        $wizard =& $this->GetWizard();
        if(!is_numeric($wizard->GetVar("left_operand"))) {
            $this->SetError("Ошибка! Вы ввели не число");
        }
    }

}
class InputRightOperand extends CWizardStep{

    // Инициализация
    function InitStep(){
        // ID шага
        $this->SetStepID("input_right_operand");
        // Заголовок
        $this->SetTitle("Левый операнд");
        // Навигация
        $this->SetPrevStep("input_left_operand");
        $this->SetNextStep("validation");
    }

    function ShowStep(){
        $this->content .= "Введите второе число: ";
        $this->content .= $this->ShowInputField("text", "right_operand");
    }

    function OnPostForm(){
        $wizard =& $this->GetWizard();
        if(!is_numeric($wizard->GetVar("right_operand"))) {
            $this->SetError("Ошибка! Вы ввели не число");
        }
    }

}
class Validation extends CWizardStep
{

    // Инициализация
    function InitStep(){
        // ID шага
        $this->SetStepID("validation");
        // Заголовок
        $this->SetTitle("Проверка");
        // Навигация
        $this->SetPrevStep("input_right_operand");
    }

    function ShowStep(){
        $wizard =& $this->GetWizard();
        $operator = $wizard->GetVar("operator");
        $left = $wizard->GetVar("left_operand");
        $right = $wizard->GetVar("right_operand");
        $this->content .= "Ваше выражение: ";
        $this->content .= $left." ";
        $this->content .= $operator." ";
        $this->content .= $right." ";
        if($operator == "/" and intval($right) == 0) {
            $this->content .= "не корректно!";
            $this->SetError("Ошибка! Деление на ноль.");
        }else{
            $this->SetFinishStep("calculate");
            $this->SetFinishCaption("Посчитать");
        }
    }
}



class Calculate extends CWizardStep{

    // Инициализация
    function InitStep(){
        // ID шага
        $this->SetStepID("calculate");
        // Заголовок
        $this->SetTitle("Результат");
        // Навигация
        $this->SetCancelStep("calculate");
        $this->SetCancelCaption("Завершить");
    }

    function ShowStep(){

        $this->content .= $this->CalculateExpr();
    }

    function OnPostForm(){
        CEventLog::Add(array(
            "SEVERITY" => "SECURITY",
            "AUDIT_TYPE_ID" => "CALCULATOR_RESULT",
            "MODULE_ID" => "main",
            "ITEM_ID" => "",
            "DESCRIPTION" =>  $this->CalculateExpr()
        ));
    }

    function CalculateExpr(){
        $wizard =& $this->GetWizard();
        $operator = $wizard->GetVar("operator");
        $left = $wizard->GetVar("left_operand");
        $right = $wizard->GetVar("right_operand");
        $result = 0;
        switch($operator){
            case "+":
                $result = $left + $right;
                break;
            case "-":
                $result = $left - $right;
                break;
            case "*":
                $result = $left * $right;
                break;
            case "/":
                $result = $left / $right;
                break;
        }
        $text = "Результат: ";
        $text .= $left." ";
        $text .= $operator." ";
        $text .= $right." = ";
        $text .= $result;
        return $text;
    }

}
