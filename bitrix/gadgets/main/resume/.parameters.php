<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::includeModule("iblock")) {
    return;
}

$arParameters = Array(
    "USER_PARAMETERS"=> Array(
        "WEBFORM_ID" => array(
            "NAME" => GetMessage("GD_RESUME_WEBFORM_ID"),
            "TYPE" => "STRING",
            "DEFAULT" => "",
        ),
        "RESUME_URL" => array(
            "NAME" => GetMessage("GD_RESUME_LIST_URL"),
            "TYPE" => "STRING",
            "DEFAULT" => "",
        ),
    ),
);
