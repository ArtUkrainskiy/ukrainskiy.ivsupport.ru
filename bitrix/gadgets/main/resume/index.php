<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!CModule::IncludeModule("form")) {
    ShowError(GetMessage("FORM_MODULE_NOT_INSTALLED"));
    return;
}

$arFilterToday = array (
    "DATE_CREATE_1" =>  date("d.m.Y", mktime()),
);
$rsResultsToday = CFormResult::GetList(1, ($by="s_timestamp"), ($order="desc"), $arFilterToday, $is_filtered, 'N', false);
$rsResultsAllTime = CFormResult::GetList(1, ($by="s_timestamp"), ($order="desc"), array(), $is_filtered, 'N', false);
if($arGadgetParams["RESUME_URL"]):
?>
    Резюме за сегодня: <a href="<?=$arGadgetParams["RESUME_URL"]?>"><?=$rsResultsToday->SelectedRowsCount()?></a><br>
    Резюме за все время: <a href="<?=$arGadgetParams["RESUME_URL"]?>"><?=$rsResultsToday->SelectedRowsCount()?></a><br>
<?else:?>
    Резюме за сегодня: <a href="/bitrix/admin/form_result_list.php?PAGEN_1=1&SIZEN_1=20&lang=ru&WEB_FORM_ID=<?=$arGadgetParams["WEBFORM_ID"]?>&set_filter=Y&adm_filter_applied=1&find_date_create_1_FILTER_PERIOD=day&find_date_create_1_FILTER_DIRECTION=current&find_date_create_1=<?=date("d.m.Y", mktime())?>&find_date_create_2=<?=date("d.m.Y", mktime())?>"><?=$rsResultsToday->SelectedRowsCount()?></a><br>
    Резюме за все время: <a href="/bitrix/admin/form_result_list.php?lang=ru&del_filter=Y&WEB_FORM_ID=<?=$arGadgetParams["WEBFORM_ID"]?>"><?=$rsResultsToday->SelectedRowsCount()?></a><br>

<?endif?>