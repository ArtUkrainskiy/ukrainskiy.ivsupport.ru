<?php
AddEventHandler("main", "OnBeforeUserUpdate",  array("CMainEventHandler", "OnBeforeUserUpdateHandler"));
AddEventHandler("main", "OnBeforeGroupUpdate", array("CMainEventHandler", "OnBeforeGroupUpdate"));

class CMainEventHandler
{
    // создаем обработчик события "OnBeforeUserUpdate"
    function OnBeforeUserUpdateHandler(&$arFields)
    {
        $isOldContentEdior = false;
        $isNewContentEditor = false;

        $rsGroups = CUser::GetUserGroupList($arFields["ID"]);
        while ($arGroup = $rsGroups->Fetch()) {
            if ($arGroup["ID"] == GROUP_CONTENT_EDITORS_ID) {
                $isOldContentEdior = true;
                break;
            }
        }

        foreach ($arFields["GROUP_ID"] as $arGroup) {
            if ($arGroup["GROUP_ID"] == GROUP_CONTENT_EDITORS_ID) {
                $isNewContentEditor = true;
                break;
            }
        }

        if (!$isOldContentEdior and $isNewContentEditor) {
            CEventLog::Add(array(
                "SEVERITY" => "SECURITY",
                "AUDIT_TYPE_ID" => "USER_GROUP_UPDATE",
                "MODULE_ID" => "iblock",
                "ITEM_ID" => "",
                "DESCRIPTION" => "Пользователь с id {$arFields["ID"]} добавлен в группу Контент-редактороы."
            ));
        }
    }


    function OnBeforeGroupUpdate($ID, &$arFields)
    {
        if (!$ID == GROUP_CONTENT_EDITORS_ID) {
            return true;
        }

        foreach ($arFields["USER_ID"] as $arUserId) {
            $isOldContentEdior = false;
            $rsGroups = CUser::GetUserGroupList($arUserId["USER_ID"]);
            while ($arGroup = $rsGroups->Fetch()) {
                if ($arGroup["GROUP_ID"] == GROUP_CONTENT_EDITORS_ID) {
                    $isOldContentEdior = true;
                    break;
                }
            }
            if (!$isOldContentEdior) {
                CEventLog::Add(array(
                    "SEVERITY" => "SECURITY",
                    "AUDIT_TYPE_ID" => "USER_GROUP_UPDATE",
                    "MODULE_ID" => "iblock",
                    "ITEM_ID" => "",
                    "DESCRIPTION" => "Пользователь с id {$arUserId["USER_ID"]} добавлен в группу Контент-редактороы."
                ));
            }
        }
    }
}