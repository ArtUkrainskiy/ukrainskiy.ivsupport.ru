<?php
function AgentEventChecker()
{
    if (CModule::IncludeModule("iblock")) {

        $arSelect = Array("ID", "NAME", "CODE", "ACTIVE_TO", "ACTIVE_FROM");
        $arFilter = Array(
            "IBLOCK_ID" => 7,
            "<DATE_ACTIVE_TO" => date("d.m.Y", mktime())
        );
        $rsResCat = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        $count = 0;
        $mail_text = "";
        while ($arItem = $rsResCat->GetNext()) {
            $count++;
            $mail_text .= $arItem["NAME"] . " ";
        }

        if ($count > 0) {
            CEventLog::Add(array(
                "SEVERITY" => "SECURITY",
                "AUDIT_TYPE_ID" => "CHECK_OLD_EVENTS",
                "MODULE_ID" => "iblock",
                "ITEM_ID" => "",
                "DESCRIPTION" => "Закончился срок действия акций: " . $mail_text
            ));

            $arFilter = Array(
                "GROUPS_ID" => Array(1)
            );

            $rsUsers = CUser::GetList(($by = "personal_country"), ($order = "desc"), $arFilter);
            $arEmail = array();
            while ($arResUser = $rsUsers->GetNext()) {
                $arEmail[] = $arResUser["EMAIL"];
            }
            if (count($arEmail) > 0) {
                $arEventFields = array(
                    "TEXT" => $mail_text,
                    "EMAIL" => implode(", ", $arEmail)
                );
                CEvent::Send("CHRECK_OLD_EVENTS", "s1", $arEventFields);
            }
        }
    }
    return "AgentEventChecker();";
}
?>