<?php
AddEventHandler("iblock", "OnBeforeIBlockElementDelete", Array("CIBlockEventHandler", "OnBeforeIBlockElementDeleteHandler"));
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("CIBlockEventHandler", "OnBeforeIBlockElementUpdateHandler"));

class CIBlockEventHandler{

    function OnBeforeIBlockElementDeleteHandler($ID){
        global $APPLICATION;
        $admin_exception = new CAdminException();
        $result = CIBlockElement::GetByID($ID);
        CModule::IncludeModule("iblock");
        if($arItem = $result->GetNext() and
            $arItem["IBLOCK_ID"] == IBLOCK_CATALOG_ID and
            $arItem["SHOW_COUNTER"] > 0)
        {
            echo $ID;
            $element = new CIBlockElement;
            echo $element->Update($ID, Array("ACTIVE" => "N"));


            $exception_message = "Количество просмотров элемента {$arItem['NAME']} - {$arItem["SHOW_COUNTER"]}. Элемент деактивирован.";
            $admin_exception->addMessage(array("text" => $exception_message));
            $APPLICATION->ThrowException($admin_exception);
            return false;
        }
    }

    function OnBeforeIBlockElementUpdateHandler(&$arFields){
        global $APPLICATION;
        $admin_exception = new CAdminException();
        if ($arFields["IBLOCK_ID"] == IBLOCK_NEWS_ID and $arFields["ACTIVE"] == "N") {
            $arSelect = Array("ID", "DATE_ACTIVE_FROM");
            $arFilter = Array(
                "ID" => $arFields["ID"],
                "ACTIVE" => "Y",
                //">DATE_ACTIVE_FROM" => date("d.m.Y", mktime(0,0,0,date("m"),date("d")-3,date("Y")))
            );
            $rsResCat = CIBlockElement::GetList(Array(), $arFilter, array(), false, $arSelect);
            $item = $rsResCat->getNext();
            $diff = time() - strtotime($item["DATE_ACTIVE_FROM"]);

            if($item and $diff < 60*60*24*3){
                $exception_message = "С момента публикации не прошло 3 дня.";
                $admin_exception->addMessage(array("text" => $exception_message));
                $APPLICATION->ThrowException($admin_exception);
                return false;
            }
        }
    }
}


?>