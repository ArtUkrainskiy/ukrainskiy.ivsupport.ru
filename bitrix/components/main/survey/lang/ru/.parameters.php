<?php
$MESS["IBLOCK_TYPE"] = "Тип инфоблока";
$MESS["IBLOCK_SURVEY_RESULT"] = "Инфоблок результатов опроса";
$MESS["IBLOCK_RESPONDENTS"] = "Инфоблок респондентов";
$MESS["IBLOCK_PROPERTY"] = "Какие поля выводить";
$MESS["CAPTCHA"] = "Использовать защиту от автоматических сообщений (CAPTCHA) для неавторизованных пользователей:";