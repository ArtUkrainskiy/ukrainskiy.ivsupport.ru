<?php
if(!CModule::IncludeModule("iblock")) {
    return;
}
$arIBlockType = CIBlockParameters::GetIBlockTypes();

$rsIBlock = CIBlock::GetList(Array("sort" => "asc"), Array("TYPE" => $arCurrentValues["IBLOCK_TYPE"], "ACTIVE"=>"Y"));
while($arr=$rsIBlock->Fetch()){
    $arIBlock[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];
}

$arComponentParameters = array(
    "PARAMETERS" => array(
        "IBLOCK_TYPE" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("IBLOCK_TYPE"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlockType,
            "REFRESH" => "Y",
        ),
        "IBLOCK_SURVEY_RESULT_ID" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("IBLOCK_SURVEY_RESULT"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlock,
            "REFRESH" => "Y",
        ),
        "IBLOCK_RESPONDENTS_ID" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("IBLOCK_RESPONDENTS"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlock,
            "REFRESH" => "Y",
        ),
        "USE_CAPTCHA" => Array(
            "NAME" => GetMessage("CAPTCHA"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
            "PARENT" => "BASE",
        ),

    ),
);

?>
