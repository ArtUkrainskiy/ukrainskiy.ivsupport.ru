<?if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die(); ?>
<a href="?results">Посмотреть результаты</a><br>
<?
if(!empty($arResult["ERROR_MESSAGE"])) {
    foreach($arResult["ERROR_MESSAGE"] as $v)
        ShowError($v);
}
if(strlen($arResult["OK_MESSAGE"]) > 0) {
    echo $arResult["OK_MESSAGE"]."<br><br>";
}
?>
<form action="<?=POST_FORM_ACTION_URI?>" method="post">
    <?=bitrix_sessid_post()?>
    <?=GetMessage("FIELD_NAME")?>:
    <input type="text" name="NAME" value="<?=$arResult["FIELD_NAME"]?>"><br>
    <?=GetMessage("FIELD_AGE")?>:
    <input type="text" name="AGE" value="<?=$arResult["FIELD_AGE"]?>"><br>
    <?=GetMessage("FIELD_GENDER")?>:
    <select name="GENDER">
        <option value="M" <?=$arResult["FIELD_GENDER"] == "M" ? "selected" : ""?>><?=GetMessage("FIELD_GENDER_MALE")?></option>
        <option value="W" <?=$arResult["FIELD_GENDER"] == "W" ? "selected" : ""?>><?=GetMessage("FIELD_GENDER_FEMALE")?></option>
    </select><br>
    <?=GetMessage("FIELD_PAY")?>:
    <input type="text" name="PAY" value="<?=$arResult["FIELD_PAY"]?>"><br>
    <?if($arParams["USE_CAPTCHA"] == "Y"):?>
    <div class="mf-captcha">
        <div class="mf-text"><?=GetMessage("CAPTCHA")?></div>
        <input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
        <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA">
        <div class="mf-text"><?=GetMessage("CAPTCHA_CODE")?><span class="mf-req">*</span></div>
        <input type="text" name="captcha_word" size="30" maxlength="50" value="">
    </div>
    <?endif?>
        <input type="submit" name="submit" value="Отправить">
</form>