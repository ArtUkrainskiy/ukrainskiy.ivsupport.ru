<?
$MESS["FIELD_NAME"] = "Имя";
$MESS["FIELD_AGE"] = "Возраст";
$MESS["FIELD_GENDER"] = "Пол";
$MESS["FIELD_PAY"] = "Заработная плата";
$MESS["FIELD_GENDER_MALE"] = "Мужчина";
$MESS["FIELD_GENDER_FEMALE"] = "Женщина";
$MESS["CAPTCHA"] = "Защита от автоматических сообщений";
$MESS["CAPTCHA_CODE"] = "Введите слово на картинке";
?>