<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arParams["USE_CAPTCHA"] = (($arParams["USE_CAPTCHA"] != "N" && !$USER->IsAuthorized()) ? "Y" : "N");


if ($_SERVER["REQUEST_METHOD"] == "POST" and $_POST["submit"] <> '' and check_bitrix_sessid()) {
    $arResult["ERROR_MESSAGE"] = array();

    //Проверка заполнения полей
    if (strlen($_POST["NAME"]) <= 1) {
        $arResult["ERROR_MESSAGE"][] = GetMessage("REQ_NAME");
    } else {
        $arResult["FIELD_NAME"] = $_POST["NAME"];
    }
    if (strlen($_POST["AGE"]) <= 1) {
        $arResult["ERROR_MESSAGE"][] = GetMessage("REQ_AGE");
    } else {
        $arResult["FIELD_AGE"] = intval($_POST["AGE"]);
    }
    if (strlen($_POST["GENDER"]) == 0) {
        $arResult["ERROR_MESSAGE"][] = GetMessage("REQ_GENDER");
    } else {
        $arResult["FIELD_GENDER"] = $_POST["GENDER"];
    }
    if (!isset($_POST["PAY"]) or intval($_POST["PAY"]) <= 0) {
        $arResult["ERROR_MESSAGE"][] = GetMessage("REQ_PAY");
    } else {
        $arResult["FIELD_PAY"] = intval($_POST["PAY"]);
    }

    //Проверка капчи
    if ($arParams["USE_CAPTCHA"] == "Y") {
        include_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/classes/general/captcha.php");
        $captcha_code = $_POST["captcha_sid"];
        $captcha_word = $_POST["captcha_word"];
        $cpt = new CCaptcha();
        $captchaPass = COption::GetOptionString("main", "captcha_password", "");
        if (strlen($captcha_word) > 0 && strlen($captcha_code) > 0) {
            if (!$cpt->CheckCodeCrypt($captcha_word, $captcha_code, $captchaPass)) {
                $arResult["ERROR_MESSAGE"][] = GetMessage("CAPTCHA_WRONG");
            }
        } else {
            $arResult["ERROR_MESSAGE"][] = GetMessage("CAPTHCA_EMPTY");
        }
    }

    //Если нет ошибок
    if (empty($arResult["ERROR_MESSAGE"])) {
        if (!CModule::IncludeModule('iblock')) {
            return;
        }

        $oElement = new CIBlockElement;
        $respondent_id = $oElement->add(array(
                "IBLOCK_ID" => $arParams["IBLOCK_RESPONDENTS_ID"],
                "NAME" => $arResult["FIELD_NAME"],
            )
        );
        echo $oElement->LAST_ERROR;

        if ($respondent_id) {
            $property_enums = CIBlockPropertyEnum::GetList(
                array("SORT" => "ASC"),
                array(
                    "IBLOCK_ID" => $arParams["IBLOCK_SURVEY_RESULT_ID"],
                    "CODE" => "GENDER"
                )
            );

            $arGenderValues = array();
            while ($field = $property_enums->GetNext()) {
                $arGenderValues[$field["XML_ID"]] = $field["ID"];
            }

            $result = $oElement->add(array(
                "IBLOCK_ID" => $arParams["IBLOCK_SURVEY_RESULT_ID"],
                "NAME" => $arResult["FIELD_NAME"],
                "PROPERTY_VALUES" => array(
                    "RESPONDENT" => $respondent_id,
                    "AGE" => $arResult["FIELD_AGE"],
                    "GENDER" => array(
                        "VALUE" => $arGenderValues[$arResult["FIELD_GENDER"]]
                    ),
                    "PAY" => $arResult["FIELD_PAY"],
                )
            ));
            echo $oElement->LAST_ERROR;
            if (!$result) {
                $arResult["ERROR_MESSAGE"][] = GetMessage("SAVE_ERROR");
            } else {
                $arResult["OK_MESSAGE"] = GetMEssage("OK_TEXT");
            }
        } else {
            $arResult["ERROR_MESSAGE"][] = GetMessage("SAVE_ERROR");
        }
    }
}


if ($arParams["USE_CAPTCHA"] == "Y")
    $arResult["capCode"] = htmlspecialcharsbx($APPLICATION->CaptchaGetCode());

$this->IncludeComponentTemplate();