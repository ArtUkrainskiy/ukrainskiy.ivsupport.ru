<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Список вакансий компании",
	"DESCRIPTION" => GetMessage("T_IBLOCK_DESC_CAT_DESC"),
	"ICON" => "/images/photo_view.gif",
	"COMPLEX" => "Y",
	"PATH" => array(
		"ID" => "content",
		"CHILD" => array(
			"ID" => "catalog_ext",
			"NAME" => GetMessage("T_IBLOCK_DESC_CAT"),
			"SORT" => 20,
		)
	),
);

?>