<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arDefaultUrlTemplates404 = array(
	"detail" => "#ELEMENT_ID#/",
);

$arDefaultVariableAliases404 = array();

$arDefaultVariableAliases = array();

$arComponentVariables = array(
	"ELEMENT_ID",
	"ELEMENT_CODE",
);
$componentPage = "";

if($arParams["SEF_MODE"] == "Y") {
	$arVariables = array();

	$arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
	$arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);

	$engine = new CComponentEngine($this);
	if (CModule::IncludeModule('iblock')){
		$engine->addGreedyPart("#SECTION_CODE_PATH#");
		$engine->setResolveCallback(array("CIBlockFindTools", "resolveComponentEngine"));
	}

	$componentPage = $engine->guessComponentPath(
		$arParams["SEF_FOLDER"],
		$arUrlTemplates,
		$arVariables
	);
	CComponentEngine::InitComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);
	$arResult = array(
		"FOLDER" => $arParams["SEF_FOLDER"],
		"URL_TEMPLATES" => $arUrlTemplates,
		"VARIABLES" => $arVariables,
		"ALIASES" => $arVariableAliases,
	);

	if(!$componentPage){
		$componentPage = "vacancies";
	}elseif($componentPage == "resume") {
		$arSelect = array("ID", "NAME");
		$arFilter = array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ID" => $arVariables["ELEMENT_ID"],
		);
		$rsIBlockElement = CIBlockElement::GetList(false, $arFilter, false, false, $arSelect);
		if($arItem = $rsIBlockElement->GetNext()) {
			$arResult["VACANCY"] = $arItem;
		}
	}
}




$this->IncludeComponentTemplate($componentPage);

?>
