<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arResult["PROPERTIES"] = array();
if (!CModule::IncludeModule('iblock')) {
    return;
}
$arSort = array("sort" => "asc");
$arFilter = array(
    "IBLOCK_ID" => $arParams["IBLOCK_SURVEY_RESULT_ID"],
);

$rsProp = CIBlockProperty::GetList($arSort, $arFilter);
$arFilter = array();
while ($arProperty = $rsProp->Fetch()) {
    if (in_array($arProperty["ID"], $arParams["PROPERTY_CODES"])) {
        if($arProperty["PROPERTY_TYPE"] == "N"){
            if($_POST[$arProperty["CODE"]."_MIN"] != "") {
                $arResult["FIELD_".$arProperty["CODE"]."_MIN"] = intval($_POST[$arProperty["CODE"]."_MIN"]);
                $arFilter[">=PROPERTY_".$arProperty["CODE"]] = intval($_POST[$arProperty["CODE"]."_MIN"]);
            }
            if($_POST[$arProperty["CODE"]."_MAX"] != "") {
                $arResult["FIELD_".$arProperty["CODE"]."_MAX"] = intval($_POST[$arProperty["CODE"]."_MAX"]);
                $arFilter["<=PROPERTY_".$arProperty["CODE"]] = intval($_POST[$arProperty["CODE"]."_MAX"]);
            }
        }
        if ($arProperty["PROPERTY_TYPE"] == "L") {
            $property_enums = CIBlockPropertyEnum::GetList(
                array("SORT" => "ASC"),
                array(
                    "IBLOCK_ID" => $arParams["IBLOCK_SURVEY_RESULT_ID"],
                    "CODE" => $arProperty["CODE"]
                )
            );
            $arGenderValues = array();
            while ($field = $property_enums->GetNext()) {
                $arProperty["LIST_VALUES"][] = $field;
            }
            if($_POST[$arProperty["CODE"]] != "") {
                $arResult["FIELD_".$arProperty["CODE"]] = intval($_POST[$arProperty["CODE"]]);
                $arFilter["PROPERTY_".$arProperty["CODE"]] = intval($_POST[$arProperty["CODE"]]);

            }
        }
        $arResult["PROPERTIES"][] = $arProperty;
    }
}
$arSort = array();
$arFilter["IBLOCK_ID"] = $arParams["IBLOCK_SURVEY_RESULT_ID"];
$arFields = array();
$respondents = CIBlockElement::GetList($arSort, $arFilter, false, false, $arFields);

$arResult["RESPONDENT_COUNT"] = $respondents->SelectedRowsCount();

$this->IncludeComponentTemplate();