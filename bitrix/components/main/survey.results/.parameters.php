<?php
if(!CModule::IncludeModule("iblock")) {
    return;
}
$arIBlockType = CIBlockParameters::GetIBlockTypes();

$rsIBlock = CIBlock::GetList(Array("sort" => "asc"), Array("TYPE" => $arCurrentValues["IBLOCK_TYPE"], "ACTIVE"=>"Y"));
while($arr=$rsIBlock->Fetch()){
    $arIBlock[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];
}

$arProperty_LNSF = array();

$rsProp = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arCurrentValues["IBLOCK_SURVEY_RESULT_ID"]));
while ($arr=$rsProp->Fetch()) {
    $arProperty[$arr["ID"]] = $arr["NAME"];
    $arProperty_LNSF[$arr["ID"]] = $arr["NAME"];
}


$arComponentParameters = array(
    "PARAMETERS" => array(
        "IBLOCK_TYPE" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("IBLOCK_TYPE"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlockType,
            "REFRESH" => "Y",
        ),
        "IBLOCK_SURVEY_RESULT_ID" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("IBLOCK_SURVEY_RESULT"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlock,
            "REFRESH" => "Y",
        ),
        "PROPERTY_CODES" => array(
            "PARENT" => "FIELDS",
            "NAME" => GetMessage("IBLOCK_PROPERTY"),
            "TYPE" => "LIST",
            "MULTIPLE" => "Y",
            "VALUES" => $arProperty_LNSF,
        ),

    ),
);

?>
