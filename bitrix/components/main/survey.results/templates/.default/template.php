<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<a href="?">Форма опроса</a><br>
<form action="<?= POST_FORM_ACTION_URI ?>" method="post">
    <?= bitrix_sessid_post() ?>
    <? foreach ($arResult["PROPERTIES"] as $arProperty): ?>
        <? if ($arProperty["PROPERTY_TYPE"] == "N"): ?>
            <?= $arProperty["NAME"] ?>:<br>
            От <input type="text" name="<?= $arProperty["CODE"] ?>_MIN" value="<?=$arResult["FIELD_".$arProperty["CODE"]."_MIN"]?>">
            До <input type="text" name="<?= $arProperty["CODE"] ?>_MAX" value="<?=$arResult["FIELD_".$arProperty["CODE"]."_MAX"]?>">
            <br>
        <? endif; ?>
        <? if ($arProperty["PROPERTY_TYPE"] == "L"): ?>
            <?= $arProperty["NAME"] ?>:<br>
            <select name="<?= $arProperty["CODE"] ?>">
                <option value="">Все</option>
                <? foreach ($arProperty["LIST_VALUES"] as $arValue): ?>
                    <option value="<?= $arValue["ID"] ?>" <?=$arResult["FIELD_".$arProperty["CODE"]]==$arValue["ID"]?"selected":""?>>
                        <?= $arValue["VALUE"] ?>
                    </option>
                <? endforeach; ?>
            </select>
            <br>
        <? endif; ?>
    <? endforeach; ?>
    <input type="submit" name="submit" value="Отправить">
</form>
<br>
Найдено <?=$arResult["RESPONDENT_COUNT"]?> респондентов.