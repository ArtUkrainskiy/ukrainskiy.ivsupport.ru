<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!isset($arParams["CACHE_TIME"])) {
	$arParams["CACHE_TIME"] = 36000000;
}

$isNotUseCache = true;
$isCacheNotFound = false;
if($arParams["CACHE_TYPE"] != "N"){
	$isNotUseCache = false;
	$isCacheNotFound = $this->StartResultCache(false, ($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()));
}
if(!CModule::IncludeModule("iblock"))
{
	$this->AbortResultCache();
	ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
	return;
}
if($arParams['IBLOCK_ID'] > 0 and ($isNotUseCache or $isCacheNotFound)) {
	$arResult = array();
	$arBlockFilter = Array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"]
	);
	$arBlockList = CIBlockSection::GetList(array($by => $order), $arBlockFilter, true);
	while($arBlock = $arBlockList->GetNext()) {
		$arResult[$arBlock["ID"]]["CATEGORY_NAME"] = $arBlock["NAME"];

		$arSelect = array("ID", "IBLOCK_ID", "NAME", "DETAIL_TEXT", "PROPERTY_EXPIRENCE", "PROPERTY_WORK_SCHEDULE", "PROPERTY_EDUCATION");
		$arFilter = array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"SECTION_ID" => $arBlock["ID"],
			"ACTIVE_DATE" => "Y",
			"ACTIVE"=>"Y",
		);
		$rsIBlockElement = CIBlockElement::GetList(false, $arFilter, false, false, $arSelect);
		$rsIBlockElement->SetUrlTemplates($arParams["DETAIL_URL"], "", $arParams["IBLOCK_URL"]);
		while($arItem = $rsIBlockElement->GetNext()){
			$arButtons = CIBlock::GetPanelButtons(
				$arItem["IBLOCK_ID"],
				$arItem["ID"],
				0,
				array("SECTION_BUTTONS"=>false, "SESSID"=>false)
			);

			$arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
			$arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];
			$arResult[$arBlock["ID"]]["ELEMENTS"][] = $arItem;
		}
	}
	//Подключаем шаблон
	$this->IncludeComponentTemplate();
}

$arButtons = CIBlock::GetPanelButtons($arParams['IBLOCK_ID'], 0, 0, array("SECTION_BUTTONS"=>false));
$this->addIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));

?>
