<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="vc_content">
<?foreach($arResult as $arCategory):?>
	<h2><?=$arCategory["CATEGORY_NAME"]?></h2>
	<ul>
	<?foreach($arCategory["ELEMENTS"] as $arItem):?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<li id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="close"><h3><?=$arItem["NAME"]?></h3>
			<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">Подробнее</a>
			<span class="vc_showchild">Подробнее</span>
			<ul>
				<li>
					<strong>Описание вакансии:</strong>
					<br><?=$arItem["DETAIL_TEXT"]?><br>
					<strong>Опыт работы:</strong>
					<br><?=$arItem["PROPERTY_EXPIRENCE_VALUE"]?><br>
					<strong>График работы:</strong>
					<br><?=$arItem["PROPERTY_WORK_SCHEDULE_VALUE"]?><br>
					<strong>Образование:</strong>
					<br><?=$arItem["PROPERTY_EDUCATION_VALUE"]?><br>
				</li>
			</ul>
			<span class="vc_showchild-2 close">Скрыть подробности</span>
		</li>

	<?endforeach?>
	</ul>
<?endforeach;?>
</div>
