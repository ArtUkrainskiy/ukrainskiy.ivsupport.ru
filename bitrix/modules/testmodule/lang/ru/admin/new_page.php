<?php
$MESS["PAGE_TITLE"] = "Список записей";
$MESS["ACCESS_DENIED"] = "Доступ запрещен";
$MESS["T_FIELD_NAME"] = "Имя";
$MESS["T_FIELD_TIMESTAMP_X"] = "Дата";
$MESS["SAVE_ERROR"] = "Ошибка сохранения";
$MESS["DELETE_ERROR"] = "Ошибка удаления";
$MESS["NAV"] = "Навигация";
$MESS["EDIT"] = "Редактировать";
$MESS["DELETE"] = "Удалить";
$MESS["DELETE_CONF"] = "Подтвердить удаление";
$MESS["ADD"] = "Добавить";
$MESS["ADD_TITLE"] = "Добавить запись";

