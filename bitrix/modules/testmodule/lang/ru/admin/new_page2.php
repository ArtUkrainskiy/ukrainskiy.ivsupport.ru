<?php
$MESS["PAGE_TITLE_EDIT"] = "Редактирование записи";
$MESS["PAGE_TITLE_ADD"] = "Добавление записи";
$MESS["ACCESS_DENIED"] = "Доступ запрещен";
$MESS["TAB_EDIT"] = "Редактирование записи";
$MESS["TAB_NEW"] = "Добавление записи";
$MESS["TAB_EDIT_TITLE"] = "Редактирование записи";
$MESS["TAB_NEW_TITLE"] = "Добавление записи";
$MESS["T_FIELD_NAME"] = "Имя";
$MESS["T_FIELD_TIMESTAMP_X"] = "Дата";
$MESS["SAVE_ERROR"] = "Ошибка сохранения";
$MESS["GOTO_LIST"] = "Список записей";
$MESS["GOTO_LIST_TITLE"] = "Список записей";
$MESS["DELETE"] = "Удалить";
$MESS["DELETE_CONF"] = "Подтвердить удаление";
$MESS["ADD"] = "Добавить";
$MESS["ADD_TITLE"] = "Добавить запись";
$MESS["SAVED"] = "Запись сохранена";
