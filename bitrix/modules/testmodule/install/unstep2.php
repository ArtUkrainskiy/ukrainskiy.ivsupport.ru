<?php
if ($GLOBALS["error"]) {
    echo CAdminMessage::ShowMessage(Array("TYPE" => "ERROR", "MESSAGE" => "Ошибка при удалении", "HTML" => true, "DETAILS" => $GLOBALS["error"]->msg));
}else{
    echo CAdminMessage::ShowNote("Модуль успешно удален");
}
?>
<br>
<form action="<?= $APPLICATION->GetCurPage()?>">
    <input type="hidden" name="lang" value="<?echo LANG?>">
    <input type="submit" name="" value="Завершить">
<form>