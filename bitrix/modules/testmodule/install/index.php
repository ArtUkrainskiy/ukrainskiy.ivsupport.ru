<?php
IncludeModuleLangFile(__FILE__);
Class testmodule extends CModule
{
    const MODULE_ID = 'testmodule';
    var $MODULE_ID = 'testmodule';
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;

    function __construct(){
        $arModuleVersion = array();
        include(dirname(__FILE__) . "/version.php");
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME = GetMessage("testmodule_MODULE_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("testmodule_MODULE_DESC");
    }

    function InstallDB($arParams = array()){
        global $DB, $DBType, $APPLICATION;
        $errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/' . self::MODULE_ID . '/install/db/' . strtolower($DBType) . '/install.sql');
        if(!empty($errors)){
            $APPLICATION->ThrowException(implode("", $errors));
            return false;
        }
    }

    function UnInstallDB($arParams = array()){
        global $DB, $DBType, $APPLICATION;
        if ($_REQUEST['savedata'] != 'Y') {
            $errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/'.self::MODULE_ID.'/install/db/'.strtolower($DBType).'/uninstall.sql');
            if(!empty($errors)){
                $APPLICATION->ThrowException(implode("", $errors));
                return false;
            }
        }
        return true;
    }

    function InstallFiles(){
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".self::MODULE_ID."/install/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin", true);
        return true;
    }

    function UnInstallFiles(){
        DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".self::MODULE_ID."/install/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
        return true;
    }

    function InstallEvents()
    {
        RegisterModuleDependences("main", "OnPanelCreate", self::MODULE_ID, "EventHandler", "OnPanelCreateHandler");
        RegisterModuleDependences("main", "OnUserTypeBuildList", self::MODULE_ID, "MediaLibUserType", "GetUserTypeDescription");
    }


    function UnInstallEvents()
    {
        UnRegisterModuleDependences("main", "OnPanelCreate", self::MODULE_ID, "EventHandler", "OnPanelCreateHandler");
        UnRegisterModuleDependences("main", "OnUserTypeBuildList", self::MODULE_ID, "MediaLibUserType", "GetUserTypeDescription");
    }

    function DoInstall()
    {
        global $APPLICATION;
        if ($APPLICATION->GetGroupRight('main') < 'W') {
            return;
        }
        $this->InstallDB();
        $this->InstallFiles();
        $this->InstallEvents();
        $GLOBALS["error"] = $APPLICATION->GetException();
        if(!$GLOBALS["error"]) {
            RegisterModule(self::MODULE_ID);
        }
        $APPLICATION->IncludeAdminFile("Результат установки", $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".self::MODULE_ID."/install/step.php");
    }

    function DoUninstall()
    {
        global $APPLICATION;
        if ($APPLICATION->GetGroupRight('main') < 'W') {
            return;
        }

        if ($_REQUEST['step'] < 2) {
            $APPLICATION->IncludeAdminFile("Удаление модуля", $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".self::MODULE_ID."/install/unstep.php");
        } elseif ($_REQUEST['step'] == 2) {
            $this->UnInstallDB();
            $this->UnInstallFiles();
            $this->UnInstallEvents();
            $GLOBALS["error"] = $APPLICATION->GetException();
            if(!$GLOBALS["error"]) {
                UnRegisterModule(self::MODULE_ID);
            }
            $APPLICATION->IncludeAdminFile("Результат удаления", $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".self::MODULE_ID."/install/unstep2.php");
        }
    }


}

?>