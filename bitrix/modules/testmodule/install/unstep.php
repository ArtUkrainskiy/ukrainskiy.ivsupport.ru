<form action="<?echo $APPLICATION->GetCurPage()?>">
    <?=bitrix_sessid_post()?>
    <input type="hidden" name="lang" value="<?echo LANGUAGE_ID?>">
    <input type="hidden" name="id" value="testmodule">
    <input type="hidden" name="uninstall" value="Y">
    <input type="hidden" name="step" value="2">
    <?CAdminMessage::ShowMessage("Внимание!<br />Модуль будет удален из системы")?>
    <p>Вы можете сохранить данные в таблицах базы данных:</p>
    <p><input type="checkbox" name="savedata" id="savedata" value="Y" checked="checked" /><label for="savedata">Сохранить таблицы</label><br /></p>
    <input type="submit" name="inst" value="Удалить">
</form>