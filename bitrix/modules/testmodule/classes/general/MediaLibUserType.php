<?
IncludeModuleLangFile(__FILE__);
class MediaLibUserType{
    function GetUserTypeDescription(){
        return array(
            "USER_TYPE_ID" => "medialib",
            "CLASS_NAME" => "MediaLibUserType",
            "DESCRIPTION" => GetMessage("MEDIALIB_DESCRIPTION"),
            "BASE_TYPE" => "string",
        );
    }

    function GetAdminListViewHTML($arUserField, $arHtmlControl){
        if ($arHtmlControl['VALUE']) {
            $arMediaLib = CMedialibCollection::GetList($Params = array('arFilter' =>
                array(
                    'ACTIVE' => 'Y',
                    'ID' => $arHtmlControl['VALUE']
                )
            ));
            if (count($arMediaLib) > 0) {
                return $arMediaLib[0]['NAME'];
            } else return '&nbsp;';
        } else return '&nbsp;';
    }

    function GetEditFormHTML($arUserField, $arHtmlControl){
        $return = "<select name='" . $arHtmlControl['NAME'] . "' " . ($arUserField['EDIT_IN_LIST'] === 'N' ? "disabled='disabled'" : "") . "><option value=''>" . GetMessage("FGSOFT_PROP_MEDIALIB_NO") . "</option>";

        $arMediaLib = CMedialibCollection::GetList();
        foreach ($arMediaLib as $mediaLib) {
            $return .= "<option " . ($mediaLib['ID'] == $arHtmlControl["VALUE"] ? 'selected' : '') . " value='" . $mediaLib['ID'] . "'>[" . $mediaLib['ID'] . "] " . $mediaLib['NAME'] . "</option>";
        }

        $return .= "</select>";

        return $return;
    }

    function GetDBColumnType($arUserField){
        global $DB;
        switch (strtolower($DB->type)) {
            case "mysql":
                return "text";
            case "oracle":
                return "varchar2(2000 char)";
            case "mssql":
                return "varchar(2000)";
        }
    }

}
