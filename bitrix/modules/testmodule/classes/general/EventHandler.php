<?php
class EventHandler{

    function OnPanelCreateHandler(){
        global $APPLICATION;
        $APPLICATION->AddPanelButton(array(
            "HREF" => "/bitrix/admin/testmodule_new_page.php",
            "TEXT" => "Тестовый модуль",
            "ALT" => "Список записей тестового модуля",
            "MAIN_SORT" => 300,
            "SORT" => 10
        ));
    }
}
?>