<?php

CModule::AddAutoloadClasses(
    "testmodule",
    array(
        "CTesttableTable" =>  "classes/mysql/TesttableTable.php",
        "MediaLibUserType" =>  "classes/general/MediaLibUserType.php",
        "EventHandler" => "classes/general/EventHandler.php",
    )
);
