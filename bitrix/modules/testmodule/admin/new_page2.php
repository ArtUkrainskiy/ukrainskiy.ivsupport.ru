<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/testmodule/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/testmodule/prolog.php");

IncludeModuleLangFile(__FILE__);
$UserAccess = $APPLICATION->GetGroupRight("testmodule");
if ($UserAccess == "D") {
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

$ID = intval($ID);		// идентификатор редактируемой записи
$message = null;		// сообщение об ошибке
$bVarsFromForm = false; // флаг "Данные получены с формы", обозначающий, что выводимые данные получены с формы, а не из БД.

// ******************************************************************** //
//                ОБРАБОТКА ИЗМЕНЕНИЙ ФОРМЫ                             //
// ******************************************************************** //

if(
    $REQUEST_METHOD == "POST" // проверка метода вызова страницы
    &&
    ($save!="" || $apply!="") // проверка нажатия кнопок "Сохранить" и "Применить"
    &&
    $UserAccess=="W"          // проверка наличия прав на запись для модуля
    &&
    check_bitrix_sessid()     // проверка идентификатора сессии
){
    $table = new CTesttableTable;

    // обработка данных формы
    $arFields = Array(
        "NAME"    => $NAME,
    );
    // сохранение данных
    if($ID > 0){
        $res = $table->Update($ID, $arFields);
    }else {
        $ID = $table->Add($arFields)->getId();
        $res = ($ID > 0);
    }
    if($res){
        // если сохранение прошло удачно - перенаправим на новую страницу
        // (в целях защиты от повторной отправки формы нажатием кнопки "Обновить" в браузере)
        if ($apply != "") {
            // если была нажата кнопка "Применить" - отправляем обратно на форму.
            LocalRedirect("/bitrix/admin/testmodule_new_page2.php?ID=" . $ID . "&mess=ok&lang=" . LANG);
        }else {
            // если была нажата кнопка "Сохранить" - отправляем к списку элементов.
            LocalRedirect("/bitrix/admin/testmodule_new_page.php?lang=" . LANG);
        }
    }else{
        // если в процессе сохранения возникли ошибки - получаем текст ошибки и меняем вышеопределённые переменные
        if($e = $APPLICATION->GetException()) {
            $message = new CAdminMessage(GetMessage("SAVE_ERROR"), $e);
        }
        $bVarsFromForm = true;
    }
}

// ******************************************************************** //
//                ВЫБОРКА И ПОДГОТОВКА ДАННЫХ ФОРМЫ                     //
// ******************************************************************** //
$row = array();

// выборка данных
if($ID > 0){
    $table = CTesttableTable::GetByID($ID);
    if(!($row = $table->Fetch())) {
        $ID = 0;
    }
}


// ******************************************************************** //
//                ВЫВОД ФОРМЫ                                           //
// ******************************************************************** //

// установим заголовок страницы
$APPLICATION->SetTitle(($ID > 0 ? GetMessage("PAGE_TITLE_EDIT") ." ".$ID : GetMessage("PAGE_TITLE_ADD")));

// не забудем разделить подготовку данных и вывод
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

// сформируем список закладок
$aTabs = array(
    array(
        "DIV" => "edit1",
        "TAB" => $ID > 0 ? GetMessage("TAB_EDIT") : GetMessage("TAB_NEW"),
        "ICON"=>"main_user_edit",
        "TITLE"=> $ID > 0 ? GetMessage("TAB_EDIT_TITLE") : GetMessage("TAB_NEW_TITLE"),
    )
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);


// конфигурация административного меню
$aMenu = array(
    array(
        "TEXT"=>GetMessage("GOTO_LIST"),
        "TITLE"=>GetMessage("GOTO_LIST_TITLE"),
        "LINK"=>"testmodule_new_page.php?lang=".LANG,
        "ICON"=>"btn_list",
    )
);

if($ID > 0){
    $aMenu[] = array("SEPARATOR"=>"Y");
    $aMenu[] = array(
        "TEXT" => GetMessage("ADD"),
        "TITLE" => GetMessage("ADD_TITLE"),
        "LINK" => "testmodule_new_page2.php?lang=".LANG,
        "ICON" => "btn_new",
    );

    $aMenu[] = array(
        "TEXT" => GetMessage("DELETE"),
        "TITLE" => GetMessage("DELETE_TITLE"),
        "LINK" => "javascript:if(confirm('".GetMessage("DELETE_CONF")."'))window.location='testmodule_new_page.php?ID=".$ID."&action=delete&lang=".LANG."&".bitrix_sessid_get()."';",
        "ICON" => "btn_delete",
    );
}

// создание экземпляра класса административного меню
$context = new CAdminContextMenu($aMenu);

// вывод административного меню
$context->Show();
?>

<?
// если есть сообщения об ошибках или об успешном сохранении - выведем их.
if($_REQUEST["mess"] == "ok" && $ID > 0)
    CAdminMessage::ShowMessage(array("MESSAGE"=>GetMessage("SAVED"), "TYPE"=>"OK"));

if($message) {
    echo $message->Show();
}elseif($table->LAST_ERROR != "") {
    CAdminMessage::ShowMessage($table->LAST_ERROR);
}
?>

<?
// далее выводим собственно форму
?>

<form method="POST" Action="<?echo $APPLICATION->GetCurPage()?>" ENCTYPE="multipart/form-data" name="post_form">
    <?// проверка идентификатора сессии ?>
    <?echo bitrix_sessid_post();?>
    <?
    // отобразим заголовки закладок
    $tabControl->Begin();
    ?>
    <?
    //********************
    // первая закладка - форма редактирования параметров рассылки
    //********************
    $tabControl->BeginNextTab();
    ?>
    <?if($ID > 0)://Для существующего элемента выведем ID?>
    <tr>
        <td>ID:</td>
        <td><input type="text" name="ID" value="<?=$row["ID"]?>" size="30" disabled></td>
    </tr>
    <?endif?>
    <tr>
        <td><span class="required">*</span><?echo GetMessage("T_FIELD_NAME")?>:</td>
        <td><input type="text" name="NAME" value="<?=$row["NAME"]?>" size="30" maxlength="100"></td>
    </tr>
    <?if($ID > 0)://Для существующего элемента выведем дату?>
        <tr>
            <td width="40%"><?echo GetMessage("T_FIELD_TIMESTAMP_X")?>:</td>
            <td width="60%"><?=$row["TIMESTAMP_X"]->toString()?></td>
        </tr>
    <?endif?>


    <?
    // завершение формы - вывод кнопок сохранения изменений
    $tabControl->Buttons(
        array(
            "disabled"=>($UserAccess<"W"),
            "back_url"=>"testmodule_new_page.php?lang=".LANG,

        )
    );
    ?>
    <input type="hidden" name="lang" value="<?=LANG?>">
    <?if($ID>0 && !$bCopy):?>
        <input type="hidden" name="ID" value="<?=$ID?>">
    <?endif;?>
    <?
    // завершаем интерфейс закладок
    $tabControl->End();
    ?>

    <?
    // дополнительное уведомление об ошибках - вывод иконки около поля, в котором возникла ошибка
    $tabControl->ShowWarnings("post_form", $message);
    ?>
    

    <?
    // информационная подсказка
    echo BeginNote();
    ?>
    <span class="required">*</span><?echo GetMessage("REQUIRED_FIELDS")?>
    <?
    echo EndNote();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>



