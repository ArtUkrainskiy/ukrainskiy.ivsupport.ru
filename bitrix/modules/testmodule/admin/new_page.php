<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/testmodule/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/testmodule/prolog.php");
IncludeModuleLangFile(__FILE__);

$UserAccess = $APPLICATION->GetGroupRight("testmodule");
if ($UserAccess < "R") {
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

$APPLICATION->SetTitle(GetMessage("PAGE_TITLE"));

$sTableID = CTesttableTable::getTableName(); // ID таблицы

$oSort = new CAdminSorting($sTableID, "ID", "desc"); // объект сортировки
$lAdmin = new CAdminList($sTableID, $oSort); // основной объект списка

// сохранение отредактированных элементов
if($lAdmin->EditAction() && $UserAccess=="W") {
    // пройдем по списку переданных элементов
    foreach($FIELDS as $ID=>$arFields) {
        if(!$lAdmin->IsUpdated($ID)) {
            continue;
        }
        // сохраним изменения каждого элемента
        $DB->StartTransaction();
        $ID = IntVal($ID);
        $cData = new CTesttableTable;
        if(($rsData = $cData->GetByID($ID)) && ($arData = $rsData->Fetch()))        {
            foreach($arFields as $key=>$value) {
                $arData[$key] = $value;
            }
            if(!$cData->Update($ID, $arData)){
                $lAdmin->AddGroupError(GetMessage("SAVE_ERROR")." ".$cData->LAST_ERROR, $ID);
                $DB->Rollback();
            }
        }else{
            $lAdmin->AddGroupError(GetMessage("SAVE_ERROR"), $ID);
            $DB->Rollback();
        }
        $DB->Commit();
    }
}

// обработка одиночных и групповых действий
if(($arID = $lAdmin->GroupAction()) && $UserAccess=="W") {
    // если выбрано "Для всех элементов"
    if($_REQUEST['action_target'] == 'selected'){
        $cData = new CTesttableTable;
        $rsData = $cData->GetList(array($by=>$order), $arFilter);
        while($arRes = $rsData->Fetch()) {
            $arID[] = $arRes['ID'];
        }
    }
    // пройдем по списку элементов
    foreach($arID as $ID){
        if(strlen($ID)<=0) {
            continue;
        }
        $ID = IntVal($ID);
        // для каждого элемента совершим требуемое действие
        switch($_REQUEST['action']){
            // удаление
            case "delete":
                @set_time_limit(0);
                $DB->StartTransaction();
                if(!CTesttableTable::Delete($ID))
                {
                    $DB->Rollback();
                    $lAdmin->AddGroupError(GetMessage("DELETE_ERROR"), $ID);
                }
                $DB->Commit();
                break;
        }
    }
}

// выборка данных из таблицы
//Обработка фильтра
$arFilter = array();
if($del_filter != "Y") {
    if ($find_id) $arFilter["ID"] = $find_id;
    if ($find_name) $arFilter["NAME"] = $find_name;
    if ($find_timestamp_x) $arFilter["TIMESTAMP_X"] = $find_timestamp_x;
}
$cData = new CTesttableTable;
$arFilterData = array(
    "select" => array("*"),
    "filter" => $arFilter,
    "order" => array(strtoupper($by)=>$order)
);
$rsData = $cData->GetList($arFilterData);
// преобразуем список в экземпляр класса CAdminResult
$rsData = new CAdminResult($rsData, $sTableID);

// аналогично CDBResult инициализируем постраничную навигацию.
$rsData->NavStart();

// отправим вывод переключателя страниц в основной объект $lAdmin
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("NAV")));

$lAdmin->AddHeaders(array(
    array(  "id"    =>"ID",
        "content"  =>"ID",
        "sort"     =>"ID",
        "default"  =>true,
    ),
    array(  "id"    =>"NAME",
        "content"  =>GetMessage("T_FIELD_NAME"),
        "sort"     =>"NAME",
        "default"  =>true,
    ),
    array(  "id"    =>"TIMESTAMP_X",
        "content"  =>GetMessage("T_FIELD_TIMESTAMP_X"),
        "sort"     =>"TIMESTAMP_X",
        "default"  =>true,
    ),
));

while($arRes = $rsData->NavNext(true, "f_")):

    // создаем строку. результат - экземпляр класса CAdminListRow
    $row =& $lAdmin->AddRow($f_ID, $arRes);

    // далее настроим отображение значений при просмотре и редактировании списка

    // параметр NAME будет редактироваться как текст, а отображаться ссылкой
    $row->AddInputField("NAME", array("size"=>20));
    $row->AddViewField("NAME", '<a href="testmodule_new_page2.php?ID='.$f_ID.'&lang='.LANG.'">'.$f_NAME.'</a>');

    // сформируем контекстное меню
    $arActions = Array();

    // редактирование элемента
    $arActions[] = array(
        "ICON"=>"edit",
        "DEFAULT"=>true,
        "TEXT"=>GetMessage("EDIT"),
        "ACTION"=>$lAdmin->ActionRedirect("testmodule_new_page2.php?ID=".$f_ID)
    );

    // удаление элемента
    if ($UserAccess>="W")
        $arActions[] = array(
            "ICON"=>"delete",
            "TEXT"=>GetMessage("DELETE"),
            "ACTION"=>"if(confirm('".GetMessage('DELETE_CONF')."')) ".$lAdmin->ActionDoGroup($f_ID, "delete")
        );

    // применим контекстное меню к строке
    $row->AddActions($arActions);

endwhile;
// резюме таблицы
$lAdmin->AddFooter(
    array(
        array("title"=>GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value"=>$rsData->SelectedRowsCount()), // кол-во элементов
        array("counter"=>true, "title"=>GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value"=>"0"), // счетчик выбранных элементов
    )
);

// групповые действия
$lAdmin->AddGroupActionTable(Array(
    "delete"=>GetMessage("MAIN_ADMIN_LIST_DELETE"), // удалить выбранные элементы
));
// сформируем меню из одного пункта - добавление рассылки
$aContext = array(
    array(
        "TEXT"=>GetMessage("ADD"),
        "LINK"=>"testmodule_new_page2.php?lang=".LANG,
        "TITLE"=>GetMessage("ADD_TITLE"),
        "ICON"=>"btn_new",
    ),
);

// и прикрепим его к списку
$lAdmin->AddAdminContextMenu($aContext);

// альтернативный вывод
$lAdmin->CheckListMode();




require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

// опишем элементы фильтра
$FilterArr = Array(
    "find_id",
    "find_name",
);
// инициализируем фильтр
$lAdmin->InitFilter($FilterArr);

// создадим объект фильтра
$oFilter = new CAdminFilter(
    $sTableID."_filter",
    array(
        "ID",
        GetMessage("T_FIELD_NAME"),
    )
);
?>

<form name="find_form" method="get" action="<?echo $APPLICATION->GetCurPage();?>">
    <?$oFilter->Begin();?>
    <tr>
        <td><?="ID"?>:</td>
        <td>
            <input type="text" name="find_id" size="47" value="<?echo htmlspecialchars($find_id)?>">
        </td>
    </tr>
    <tr>
        <td><?=GetMessage("T_FIELD_NAME")?>:</td>
        <td>
            <input type="text" name="find_name" size="47" value="<?echo htmlspecialchars($find_name)?>">
        </td>
    </tr>
    <?
    $oFilter->Buttons(array("table_id"=>$sTableID,"url"=>$APPLICATION->GetCurPage(),"form"=>"find_form"));
    $oFilter->End();
    ?>
</form>
<?
// выведем таблицу списка элементов
$lAdmin->DisplayList();


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>



