<?php
IncludeModuleLangFile(__FILE__);

if($APPLICATION->GetGroupRight("testmodule") >= "R") {
    $arMenu = array(
        "parent_menu" => "global_menu_content",
        "sort" => 100,
        "text" => GetMessage("MENU_TEXT"),
        "title"=> GetMessage("MENU_TITLE"),
        "icon" => "form_menu_icon",
        "page_icon" => "form_page_icon",
        "items_id" => "testmodule_menu",
        "module_id" => "testmodule",
        "items" => array()
    );
    $arMenu["items"][] = array(
        "parent_menu" => "global_menu_content",
        "sort" => 150,
        "url" => "testmodule_new_page.php",
        "text" => GetMessage("MENU_PAGE_TEXT"),
        "title"=> GetMessage("MENU_PAGE_TITLE"),
        "icon" => "form_menu_icon",
        "page_icon" => "form_page_icon",
        "items_id" => "testmodule_menu",
        "module_id" => "testmodule",
        "items" => array()
    );
    $arMenu["items"][] = array(
        "parent_menu" => "global_menu_content",
        "sort" => 200,
        "url" => "testmodule_new_page2.php",
        "text" => GetMessage("MENU_PAGE2_TEXT"),
        "title"=> GetMessage("MENU_PAGE2_TITLE"),
        "icon" => "form_menu_icon",
        "page_icon" => "form_page_icon",
        "items_id" => "testmodule_menu",
        "module_id" => "testmodule",
        "items" => array()
    );
    return $arMenu;
}
return false;

?>